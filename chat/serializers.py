from rest_framework import serializers


class StreamSerializer(serializers.Serializer):

    number = serializers.IntegerField()
    day = serializers.DateTimeField()
